TEX=master.tex
PDF=$(TEX:.tex=.pdf)

all: $(PDF)

$(PDF): $(TEX)
	latexmk -pdf $(TEX)

clean:
	rm -f $(PDF) *.log *.bbl *.blg *.aux *.fls *.toc *.fdb*
